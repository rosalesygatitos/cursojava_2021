package practica4;
import java.util.Scanner;
public class Ejercicio15 {
	public static void main(String[] args) {
		Scanner scan=new Scanner (System.in);
		System.out.println("Ingrese una clase de veh�culo");
		char clase = scan.next().charAt(0);
		
		switch (clase)
		{
		case 'a':
			System.out.println("Posee 4 ruedas y un motor");
			break;
		case 'b':
			System.out.println("Posee 4 ruedas, un motor, cierre centralizado y aire acondicionado");
			break;
		case 'c':
			System.out.println("Posee 4 ruedas, un motor, cierre centralizado, aire acondicionado y airbag");
			break;
		default:
			System.out.println("No pertenece a una clase de veh�culo \nClases \"a\", \"b\" y \"c\"");
		}
	}
}
