package practica4;
import java.util.Scanner;
public class Ejercicio12 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese un n�mero");
		int n =scan.nextInt();
		
		if (n>0 || n<=12)
			System.out.println("El n�mero pertenece a la primera docena: " + n);
		else if (n>=13 || n<=24)
			System.out.println("El n�mero pertenece a la segunda docena: " + n);
		else if (n>=25 || n<=36)
			System.out.println("El n�mero pertenece a la tercera docena: " + n);
		else
			System.out.println("El n�mero ingresado est� fuera de rango: " + n);
	}
}
