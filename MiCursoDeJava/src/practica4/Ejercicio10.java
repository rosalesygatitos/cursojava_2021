package practica4;
import java.util.Scanner;
public class Ejercicio10 {
	public static void main(String[] args) {
		Scanner scan=new Scanner (System.in);
		System.out.println("Ingrese la primera variable");
		int v1 = scan.nextInt();
		System.out.println("Ingrese la segunda variable");
		int v2 = scan.nextInt();
		System.out.println("Ingrese la tercera variable");
		int v3 = scan.nextInt();
		
		if (v1 > v2 && v1 > v3)
			System.out.println("La mayor es la primera variable: " + v1);
		else if (v2 > v1 && v2 > v3)
			System.out.println("La mayor es la segunda variable: " + v2);
		else if (v3 > v1 && v3 > v2)
			System.out.println("La mayor es la tercera variable: " + v3);
		else
			System.out.println("Error");
		scan=null;
	}
}
