package practica4;
import java.util.Scanner;
public class Ejercicio18 {
	public static void main(String[] args) {
		System.out.println("Ingrese hasta que tabla desea ver");
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		
		for(int o=0; o<n+1; o++ )
		{
			System.out.println("Tabla del " + o);
			for(int i=1; i<11; i++ )
			{
				int resultado = o * i;
				System.out.println(o + "x" + i + "=" + resultado);
			}
			System.out.println("");
		}
	}
}
