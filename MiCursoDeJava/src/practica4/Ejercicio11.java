package practica4;
import java.util.Scanner;
public class Ejercicio11 {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Ingrese cualquier letra");
		char letra = scan.next().charAt(0);
		
		if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u' || letra == 'A' || letra == 'E' || letra == 'I' || letra == 'O' || letra == 'U')
			System.out.println("La letra es una vocal");
		else
			System.out.println("La letra no es una vocal");
		scan=null;
	}
}
