package practica4;
import java.util.Scanner;
public class Ejercicio5 {
	public static void main(String[] args) {
		Scanner scan=new Scanner (System.in);
		System.out.println("Ingrese la posici�n del participante");
		int posicion = scan.nextInt();
		if(posicion == 1)
			System.out.println("�Ganador de la medalla de oro!");
		else if(posicion == 2)
			System.out.println("Ganador de la medalla de plata");
		else if(posicion == 3)
			System.out.println("Ganador de la medalla de bronce...");
		else
			System.out.println("Siga participando, lo esperamos en el pr�ximo evento");
		scan=null;
	}
}
