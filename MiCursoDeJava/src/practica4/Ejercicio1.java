package practica4;

import java.util.Scanner;

public class Ejercicio1 {
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.println("Ingrese la primer nota");
		float nota1=scan.nextFloat();
		System.out.println("Ingrese la segunda nota");
		float nota2=scan.nextFloat();
		System.out.println("Ingrese la tercer nota");
		float nota3=scan.nextFloat();
		
		float prom= (nota1+nota2+nota3)/3;
		
		if (prom >=7 )
			System.out.println("Big :D, aprob� con un promedio de " + prom);
		else
			System.out.println("Big :C, desaprob� con un promedio de " + prom);
	}
}
