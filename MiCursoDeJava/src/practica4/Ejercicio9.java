package practica4;
import java.util.Scanner;
public class Ejercicio9 {
	public static void main(String[] args) {
		Scanner scan=new Scanner (System.in);
		System.out.println("Ingrese \"0\" para piedra, \"1\" para papel, y \"2\" para tijera");
		System.out.println("Jugador 1");
		int v1 = scan.nextInt();
		System.out.println("Ingrese \"0\" para piedra, \"1\" para papel, y \"2\" para tijera");
		System.out.println("Jugador 2");
		int v2 = scan.nextInt();
		if (v1 == v2)
			System.out.println("Empataron");
		else
		{
			if (v1 == 0 && v2 == 2)
				System.out.println("Gana el jugador 1");
			else if (v1 == 1 && v2 == 0)
				System.out.println("Gana el jugador 1");
			else if (v1 == 2 && v2 == 1)
				System.out.println("Gana el jugador 1");
			else if (v1 == 0 && v2 == 1)
				System.out.println("Gana el jugador 2");
			else if (v1 == 1 && v2 == 2)
				System.out.println("Gana el jugador 2");
			else if (v1 == 2 && v2 == 0)
				System.out.println("Gana el jugador 2");
		}
	}
}
