package modulo1;

public class EjemplosDeVariables {
	public static void main(String[] args) {
		byte b = (byte)200;
		int cantidad1 = 2;
		int cantidad2 = 4;
		int cantidad3 = 22;
		int CantTotal = cantidad1 + 
						cantidad2 +
						cantidad3;
		float PromCant = (float)CantTotal/3;
		float temperatura = 22.2f;
		boolean isAlerta = false;
		
		System.out.println("La cantidad es = \t" + cantidad1);
		System.out.println("La temperatura es = \t" + temperatura);
		System.out.println("isAlerta = \t\t" + isAlerta);
		System.out.println("La cantidad total es = \t" + CantTotal);
		System.out.println("El promedio es = \t" + PromCant);
		System.out.println("El valor de b = \t\t" + b);
	
	}
}
