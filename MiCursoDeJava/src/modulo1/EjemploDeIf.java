package modulo1;

public class EjemploDeIf {

	public static void main(String[] args) {
		float nota1 =7f;
		float nota2 =4.5f;
		float nota3 =(float)8.3;
		
		float promedio =(nota1+nota2+nota3)/3;
		System.out.println("La nota 1 es " + nota1);
		System.out.println("La nota 2 es " + nota2);
		System.out.println("La nota 3 es " + nota3);
		if (promedio >=7 )
			System.out.println("Aprobado :D  \nLa nota es " + promedio);
		else
			System.out.println("Desaprobado :c \nLa nota es " + promedio);
	}
}
