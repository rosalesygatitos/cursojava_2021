package practica1;

public class Ejercicio3 {
	public static void main(String[] args) {
		System.out.println("Tecla de escape \t\t\tSignificado");
		System.out.println("\\n \t\t\tSignifica una nueva l�nea");
		System.out.println("\\t \t\t\tSignifica un TAB de espacio");
		System.out.println("\\\" \t\t\tEs para poner \" (comillas dobles) dentro de un texto. Ejemplo: \"Hola\"");
		System.out.println("\\\\ \t\t\tSe utiliza para escribir la \\ dentro del texto. Ejemplo:\\Hola\\");
		System.out.println("\\\' \t\t\tSe utiliza para la \' (comilla simple)dentro de un texto. Ejemplo: \'Hola\'");
	}

}
