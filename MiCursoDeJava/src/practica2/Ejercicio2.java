package practica2;

public class Ejercicio2 {
	public static void main(String[] args) {
		byte	bmin= -128;
		byte	bmax= 127;
		short	smin= -32768;
		short	smax= 32767;
		int		imin= -2147483648;
		int		imax= 2147483647;
		long	lmin= -9223372036854775808L;
		long	lmax= 9223372036854775807L;
		
		System.out.println("Tipo de dato\tM�nimo\t\t\t\tM�ximo");
		System.out.println("------------\t-------\t\t\t\t-----");
		System.out.println("\nbyte\t\t"+ bmin + "\t\t\t\t" + bmax);
		System.out.println("\nshort\t\t"+ smin + "\t\t\t\t" + smax);
		System.out.println("\nint\t\t"+ imin + "\t\t\t" + imax);
		System.out.println("\nlong\t\t"+ lmin + "\t\t" + lmax);
		System.out.println("\n�Cu�l es la f�rmula que me permite mostrar los m�nimos y los m�ximos \nteniendo en cuenta la cantidad de bits?");
		System.out.println("\nLa f�rmula general que se utiliza es 2 elevado a la cantidad de bits necesarios, \ndividido dos. Rstando -1 a la cantidad de bit maximos (ya que el \"0\" ucupa un espacio de los positivos)");
	}

}
