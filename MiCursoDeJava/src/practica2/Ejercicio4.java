package practica2;

public class Ejercicio4 {
	public static void main(String[] args) {
		byte b = 10;
		short s = 20;
		int i = 30;
		long l = 40;
		
		byte sumabb = (byte)(b+b);
		short sumabs = (short)(s+b);
		int sumabi = (b+i);
		int sumaii = (i+i);
		long sumasl = (s+l);
		System.out.println(sumabb);
		System.out.println(sumabs);
		System.out.println(sumabi);
		System.out.println(sumaii);
		System.out.println(sumasl);
		
		
		//Ejercicio5
		
		l=s;
		b=(byte)s;
		System.out.println("No es posible convertir short a byte\nCasteo: byte");
		l=i;
		b=(byte)i;
		System.out.println("No es posible convertir de int a byte\nCasteo: byte");
		s=(short)i;
		System.out.println("No es posible convertir de int a short\nCasteo: short");
	}

}
